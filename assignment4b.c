//TAKING NUMBERS UNTIL -1 ENCOUNTERED
#include <stdio.h>
int main()
{
    int n,positive=0,negative=0,zero=0;
   do
    {
        printf("Enter a number \n");
        scanf("%d", &n);

        if (n>0)
        {
            positive++;
        }
        else if (n<0)
        {
            negative++;
        }
        else
        {
            zero++;
        }
    }while(n!=-1);
    printf("\nPositive Numbers : %d\nNegative Numbers : %d\nZero Numbers : %d",positive,negative,zero);
    return 0;
}