//LARGEST OF 3 NUMBERS
#include <stdio.h>

void main()
{
    int a, b, c;
    printf("Enter any 3 numbers of your choice:  ");
    scanf("%d%d%d",&a,&b,&c);
    
    if(a>b && a>c)
    printf("Of all 3 numbers, %d is the greatest!",a);
    
    else if(b>c)
    printf("Of all 3 numbers, %d is the greatest!",b);
    
    else
    printf("Of all 3 numbers, %d is the greatest!",c);
}