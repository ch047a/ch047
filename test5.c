//Transpose of a matrix
#include <stdio.h>
void transpose();
int main() 
{
    printf("Program to display the transpose of the element.\n");
    transpose();
    return 0;
}
void transpose()
{
int a[10][10],trp_a[10][10],r,c,i,j;
    printf("How mant rows ?\n");
    scanf("%d",&r);
    printf("How many columns ?\n");
    scanf("%d",&c);
    printf("Enter matrix elements:\n");
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            printf("Enter element a[%d][%d]: ",i,j);
            scanf("%d",&a[i][j]);
        }
    }
    
    printf("\nEntered matrix: \n");
    for(i=0;i<r;i++)
    {
        for (j=0;j<c;j++)
        {
            printf("%d\t",a[i][j]);
        }
        printf("\n");
    }
	for (i=0;i<r;i++
	{
        for (j=0;j<c;j++) 
        {
            trp_a[j][i]=a[i][j];
        }
    }

    
    printf("\nTranspose of the matrix:\n");
    for (i=0;i<c;i++)
    {
        for (j=0;j<r;j++) 
        {
            printf("%d\t",trp_a[i][j]);
        }
        printf("\n");
    }
}