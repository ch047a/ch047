#include<stdio.h>
#include<string.h>
void perform();
int main()
{
    printf("Program to read and display employee information using nested structures.\n");
    perform();
    return 0;
}
void perform()
{
    struct date
    {
        int dd;
        int mm;
        int yy;
    };
    struct details
    {
        int id;
        char name[25];
        int sal;
        struct date emp;
       
    };
    struct details info;
    //Read input
    printf("Enter the employee details below.\n");
    printf("Enter the employee ID.\n");
    scanf("%d",&info.id);
    printf("Enter the employee name.\n");
    scanf("%s",&info.name);
    printf("Enter the employee salary.\n");
    scanf("%d",&info.sal);
    printf("Enter the employee date of joining (format:dd/mm/yyyy)\n");
    scanf("%d/%d/%d",&info.emp.dd,&info.emp.mm,&info.emp.yy);
    //Display output
    printf("Name->%s",info.name);
    printf("ID->%d",info.id);
    printf("Salary->%d",info.sal);
    printf("Date of joining->%d/%d/%d",info.emp.dd,info.emp.mm,info.emp.yy);
}
